from flask import Flask, render_template, request
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def home():
    error = None
    if request.method == 'POST':
        name = request.form['name']
        password = request.form['password']
        if name == 'testname':
            if password == 'testpassword':
                return render_template('welcome.html',name=name)
            else:
                error = 'The password is incorrect.'
        else:
            error = 'The username is incorrect'
    return render_template('home.html', error=error)


@app.route('/logout', methods=['GET', 'POST'])
def logout():
    return render_template('logout.html')

if __name__ == '__main__':
    app.run(debug=True)